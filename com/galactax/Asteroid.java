/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

/**
 * This class represents a mysterious space rock of varying size, drifting through the vacuum.
 * <p>
 * Especially dangerous in big packs, when poorly understood physical forces in space
 * make evasion by steering outside of The Screen near-impossible.
 *
 * @author marcin
 * @see GameObject
 */
public class Asteroid extends GameObject {
    
    Polygon collisionPolygon;
    
    Size size;
    
    /**
     * Represents one of the three asteroid sizes.
     */
    public enum Size {
	
	SMALL,
	MEDIUM,
	BIG;
	
	public static Size getFromInt(int n) {
	    
	    return values()[n];
	}
    }
    
    /**
     * 
     * @param x
     * @param y
     * @param w
     * @param h
     * @param s 
     * @see GameObject#GameObject(float, float, float, float) 
     */
    public Asteroid(float x, float y, float w, float h, Size s) {
	super(x, y, w, h);
	
	this.size = s;
	
	switch(size) {
	    
	    case SMALL:
		this.collisionPolygon = new Polygon(Constants.SMALL_VERTICES);
		break;
		
	    case MEDIUM:
		this.collisionPolygon = new Polygon(Constants.MEDIUM_VERTICES);
		break;
	
	    case BIG:
		this.collisionPolygon = new Polygon(Constants.BIG_VERTICES);
		break;
	}
	
	this.speed = new Vector2(0f, -Constants.ASTEROID_SPEEDS[size.ordinal()]);
	
	this.collisionPolygon.setPosition(rect.x, rect.y);
	this.collisionPolygon.dirty();
    }
    
    @Override
    public void applySpeed(float delta) {
	
	super.applySpeed(delta);
	
	collisionPolygon.setPosition(collisionPolygon.getX() + speed.x * delta, collisionPolygon.getY() + speed.y * delta);
    }
}
