/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import java.util.Iterator;

/**
 * This class is the pinnacle of the code - the GameScreen, where all the fun takes place.
 *
 * @author marcin
 * @see Screen
 */
public class GameScreen implements Screen {

    private final Galactax game;
    
    Texture spaceshipTexture;
    Texture laserTexture;
    Texture heartTexture;
    Texture enemyTexture;
    
    @SuppressWarnings("FieldMayBeFinal")
    private Array<Enemy> enemies;
    
    Texture enemyLaserTexture;
    
    @SuppressWarnings("FieldMayBeFinal")
    private Array<EnemyProjectile> enemyProjectiles;
    
    @SuppressWarnings("FieldMayBeFinal")
    private Array<Rectangle> lasers;
    
    Texture starsTexture;
    
    TextureRegion smallStar;
    TextureRegion bigStar;
    
    @SuppressWarnings("FieldMayBeFinal")
    private Array<Star> stars;
    
    Texture asteroidTexture;
    
    TextureRegion smallAsteroid;
    TextureRegion mediumAsteroid;
    TextureRegion bigAsteroid;
    
    @SuppressWarnings("FieldMayBeFinal")
    private Array<Asteroid> asteroids;
    
    private long lastLaserTime;
    
    private int konamiCodeProgress;
    private boolean isKonamiEntered;
    
    private static final int[] KONAMI_KEYS = {Input.Keys.UP, Input.Keys.UP,
					      Input.Keys.DOWN, Input.Keys.DOWN,
					      Input.Keys.LEFT, Input.Keys.RIGHT,
					      Input.Keys.LEFT, Input.Keys.RIGHT,
					      Input.Keys.B, Input.Keys.A};
    
    Spaceship spaceship;
    
    private static float minLaserSpawnTime;
    
    private int heartBlinkCounter;
    private static final int HEART_BLINK_FRAMES = 46;
    
    private int currentScore;
    
    private Generator generator;
    
    /**
     * Constructs a new GameScreen.
     * 
     * @param g reference to the main game class
     */
    public GameScreen(final Galactax g) {
	
	this.game = g;
	
	spaceshipTexture = new Texture("spaceship2.png");
	heartTexture = new Texture("heart.png");
	
	enemyTexture = new Texture("enemy.png");
	enemies = new Array<Enemy>();
	
	laserTexture = new Texture("blue_laser.png");
	enemyLaserTexture = new Texture("green_laser.png");
	
	lasers = new Array<Rectangle>();
	enemyProjectiles = new Array<EnemyProjectile>();
	
	starsTexture = new Texture("stars.png");
	
	smallStar = new TextureRegion(starsTexture, 0, 0, 8, 8);
	bigStar = new TextureRegion(starsTexture, 8, 0, 8, 8);
	
	stars = new Array<Star>();
	
	asteroidTexture = new Texture("asteroids.png");
	
	smallAsteroid = new TextureRegion(asteroidTexture, 0, 96, 32, 32);
	mediumAsteroid = new TextureRegion(asteroidTexture, 128, 64, 64, 64);
	bigAsteroid = new TextureRegion(asteroidTexture, 256, 0, 128, 128);
	
	asteroids = new Array<Asteroid>();
	
	lastLaserTime = TimeUtils.nanoTime();
	
	spaceship = new Spaceship(Constants.GAME_WIDTH / 2 - spaceshipTexture.getWidth() / 2, 30,
				    spaceshipTexture.getWidth(), spaceshipTexture.getWidth());
	
	spaceship.collisionRectangle = new Rectangle(spaceship.rect.x + 12, spaceship.rect.y + 6, spaceship.rect.width - 24, spaceship.rect.height - 20);
	
	konamiCodeProgress = 0;
	isKonamiEntered = false;
	
	minLaserSpawnTime = (float)(laserTexture.getHeight() * 3) / (float)Constants.PROJECTILE_SPEED;
	
	heartBlinkCounter = 0;
	
	currentScore = 0;
	
	generator = new Generator(this);
    }
    
    /**
     * Adds a new star to the game.
     * 
     * @param x
     * @param speed
     * @param small
     * @see Star
     */
    void spawnStar(float x, float speed, boolean small) {
	
	Star star = new Star(x,
			     Constants.GAME_HEIGHT,
			     starsTexture.getWidth(),
			     starsTexture.getHeight(),
			     speed,
			     small);
	
	stars.add(star);
    }
    
    /**
     * Adds a new enemy to the game.
     * 
     * @param y
     * @param left whether the enemy should spawn on the left or on the right of the screen
     * @see Enemy
     */
    void spawnEnemy(float y, boolean left) {
	
	Enemy enemy = new Enemy(left ? -enemyTexture.getWidth() : Constants.GAME_WIDTH,
				y,
				enemyTexture.getWidth(), enemyTexture.getHeight(),
				left ? Constants.ENEMY_SPEED : -Constants.ENEMY_SPEED);
	
	enemies.add(enemy);
    }
    
    /**
     * Adds a new asteroid to the game.
     * 
     * @param x
     * @param s asteroid size
     * @see Asteroid
     */
    void spawnAsteroid(float x, Asteroid.Size s) {
	
	TextureRegion region = new TextureRegion();
	
	switch(s) {
	    
	    case SMALL:
		region = smallAsteroid;
		break;
		
	    case MEDIUM:
		region = mediumAsteroid;
		break;
		
	    case BIG:
		region = bigAsteroid;
		break;
	}
	
	Asteroid asteroid = new Asteroid(x,
					 Constants.GAME_HEIGHT,
					 region.getRegionWidth(),
					 region.getRegionHeight(),
					 s);
	
	asteroids.add(asteroid);
	
	//System.out.println("Asteroid spawned: " + random);
    }
    
    /**
     * Adds a new player projectile.
     * <p>
     * It behaves similarly to the EnemyProjectile.
     * 
     */
    void shootLaser() {
	
	Rectangle laser = new Rectangle(spaceship.rect.x + spaceship.rect.width / 2 - laserTexture.getWidth() / 2,
					spaceship.rect.y + spaceship.rect.height,
					laserTexture.getWidth(), laserTexture.getHeight());
	
	lasers.add(laser);
    }
    
    /**
     * Adds a new EnemyProjectile to the game.
     * 
     * @param ex
     * @param ey 
     */
    void shootPlayer(float ex, float ey) {
	
	Vector2 seek = new Vector2(spaceship.rect.x + spaceship.rect.width / 2 - ex,
				   spaceship.rect.y + spaceship.rect.height / 2 - ey);
	
	seek.setLength(Constants.PROJECTILE_SPEED * 0.6f);
	
	EnemyProjectile en = new EnemyProjectile(ex, ey, enemyLaserTexture.getWidth(), enemyLaserTexture.getHeight(), seek);
	
	enemyProjectiles.add(en);
    }
    
    void onCollisionWithAsteroidOrLaser() {
	
	if(spaceship.getHealth() == 1)
	    game.gameOver(currentScore);
	
	spaceship.dealDamage();
    }
    
    private static boolean overlapsPolygonRect(Polygon p, Rectangle r) {
	
	Polygon rPoly = new Polygon(new float[] {0, 0, r.width, 0, r.width, r.height, 0, r.height});
	rPoly.setPosition(r.x, r.y);
	
	return Intersector.overlapConvexPolygons(rPoly, p);
    }
    
    private void draw(float delta) {
	
	game.view.render(delta);
	
	//draw game background
	game.view.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
	game.view.shapeRenderer.setColor(Constants.GAME_BACKGROUND_COLOR);
	game.view.shapeRenderer.rect(0, 0, Constants.GAME_WIDTH, Constants.GAME_HEIGHT);
	game.view.shapeRenderer.end();
	
	//RENDER BATCH BEGIN
	game.view.batch.begin();
	
	for(Star star : stars) {
	    
	    game.view.batch.draw(star.isSmall() ? smallStar : bigStar,
				 star.rect.x, star.rect.y);
	}
	
	for(Asteroid aster : asteroids) {
	    
	    //System.out.println("" + Arrays.toString(aster.collisionPolygon.getVertices()));
	    
	    switch(aster.size) {
		
		case SMALL:
		    game.view.batch.draw(smallAsteroid, aster.rect.x, aster.rect.y);
		    break;
		
		case MEDIUM:
		    game.view.batch.draw(mediumAsteroid, aster.rect.x, aster.rect.y);
		    break;
		
		case BIG:
		    game.view.batch.draw(bigAsteroid, aster.rect.x, aster.rect.y);
		    break;
	    }
	}
	
	if(spaceship.getBlinker().shouldDisplay(delta))
	    game.view.batch.draw(spaceshipTexture, spaceship.rect.x, spaceship.rect.y);
	
	for(Rectangle laser : lasers) {
	    
	    game.view.batch.draw(laserTexture, laser.x, laser.y);
	}
	
	for(EnemyProjectile enemyLaser : enemyProjectiles) {
	    
	    game.view.batch.draw(enemyLaserTexture, enemyLaser.rect.x, enemyLaser.rect.y,
				 enemyLaser.collisionPolygon.getOriginX(),
				 enemyLaser.collisionPolygon.getOriginY(),
				 enemyLaser.rect.width, enemyLaser.rect.height,
				 1.5f, 1.5f, enemyLaser.getAngle() - 90,
				 0, 0, enemyLaserTexture.getWidth(), enemyLaserTexture.getHeight(), false, false);
	}
	
	for(Enemy en : enemies) {
	    
	    game.view.batch.draw(enemyTexture, en.rect.x, en.rect.y);
	}
	
	game.view.font.draw(game.view.batch, "" + currentScore, 5, Constants.GAME_HEIGHT - game.view.font.getAscent() - 5);
	
	int x = Constants.GAME_WIDTH - Constants.PLAYER_HEALTH * (heartTexture.getWidth() + 5);
	
	++heartBlinkCounter;
	heartBlinkCounter %= HEART_BLINK_FRAMES;
	
	for(int i = 0; i < spaceship.getHealth(); ++i) {
	    
	    if(spaceship.getHealth() > 1 || heartBlinkCounter < HEART_BLINK_FRAMES / 2)
		game.view.batch.draw(heartTexture, x, Constants.GAME_HEIGHT - heartTexture.getHeight() - 5);
	    
	    x += heartTexture.getWidth() + 5;
	}
	
	game.view.batch.end();
	//RENDER BATCH END
	
	/*
	game.view.shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
	game.view.shapeRenderer.setColor(Color.RED);
	game.view.shapeRenderer.rect(spaceship.collisionRectangle.x, spaceship.collisionRectangle.y,
				     spaceship.collisionRectangle.width, spaceship.collisionRectangle.height);
	game.view.shapeRenderer.end();
	*/
    }
    
    private void update(float delta) {
	
	/*
	if(Gdx.input.isKeyJustPressed(Input.Keys.Q))
	    shootPlayer(400f, 600f);
	
	if(Gdx.input.isKeyJustPressed(Input.Keys.R))
	    spawnEnemy();
	
	if(Gdx.input.isKeyJustPressed(Input.Keys.E))
	    spawnAsteroid();
	*/
	
	if(!isKonamiEntered) {
	
	    if(Gdx.input.isKeyJustPressed(KONAMI_KEYS[konamiCodeProgress]))
	    {
		++konamiCodeProgress;

		//System.out.println("" + konamiCodeProgress);
		
		if(konamiCodeProgress == KONAMI_KEYS.length)
		{
		    System.out.println("Konami Code typed in!");

		    konamiCodeProgress = 0;
		    isKonamiEntered = true;
		}
	    }

	    else if(Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
		
		//System.out.println("Konami Code reset!");
		
		konamiCodeProgress = 0;
	    }
	}
	
	if(Gdx.input.isKeyJustPressed(Constants.PAUSE_KEY)) {
	    
	    //this.pause();
	    
	    game.pauseGame();
	}
	
	if(Gdx.input.isKeyPressed(Constants.SHOOT_KEY)) {
	    
	    //System.out.println("SPACE PRESSED");
	    
	    long timeNow = TimeUtils.nanoTime();
	    
	    if(Gdx.input.isKeyJustPressed(Constants.SHOOT_KEY)) {
		
		shootLaser();
		
		lastLaserTime = timeNow;
	    }
	    
	    else {

		if( isKonamiEntered && (timeNow - lastLaserTime > minLaserSpawnTime * 1000000000) ) {

		    shootLaser();

		    lastLaserTime = timeNow;
		}
	    }
	}
	
	//else
	//    System.out.println("SPACE NOT PRESSED");
	
	boolean hasSpaceshipMoved = false;
                
        if(Gdx.input.isKeyPressed(Constants.LEFT_KEY) &&
          !Gdx.input.isKeyPressed(Constants.RIGHT_KEY)) {
                    
            spaceship.setHorizontalSpeed(-Constants.SPACESHIP_SPEED);
	    
	    hasSpaceshipMoved = true;
        }
                
        if(Gdx.input.isKeyPressed(Constants.RIGHT_KEY) &&
          !Gdx.input.isKeyPressed(Constants.LEFT_KEY)) {
                    
            spaceship.setHorizontalSpeed(Constants.SPACESHIP_SPEED);
	    
	    hasSpaceshipMoved = true;
        }
                
        if(Gdx.input.isKeyPressed(Constants.UP_KEY) &&
          !Gdx.input.isKeyPressed(Constants.DOWN_KEY)) {
                    
            spaceship.setVerticalSpeed(Constants.SPACESHIP_SPEED);
	    
	    hasSpaceshipMoved = true;
        }
                
        if(Gdx.input.isKeyPressed(Constants.DOWN_KEY) &&
	  !Gdx.input.isKeyPressed(Constants.UP_KEY)) {
            
            spaceship.setVerticalSpeed(-Constants.SPACESHIP_SPEED);
	    
	    hasSpaceshipMoved = true;
        }
	
	spaceship.applySpeed(delta);
	spaceship.resetSpeed();
	
	if(hasSpaceshipMoved) {
	    
	    if(spaceship.rect.x < 0) {
		
		spaceship.rect.x = 0;
		spaceship.collisionRectangle.x = 12f;
	    }

	    else if(spaceship.rect.x > Constants.GAME_WIDTH - spaceship.rect.width) {
		
		spaceship.rect.x = Constants.GAME_WIDTH - spaceship.rect.width;
		spaceship.collisionRectangle.x = Constants.GAME_WIDTH - spaceship.collisionRectangle.getWidth() - 12f;
	    }

	    if(spaceship.rect.y < 0) {
		
		spaceship.rect.y = 0;
		spaceship.collisionRectangle.y = 6f;
	    }

	    else if(spaceship.rect.y > Constants.GAME_HEIGHT - spaceship.rect.height) {
		
		spaceship.rect.y = Constants.GAME_HEIGHT - spaceship.rect.height;
		spaceship.collisionRectangle.y = Constants.GAME_HEIGHT - spaceship.collisionRectangle.height - 14f;
	    }
	}
	
	//if(TimeUtils.nanoTime() - lastStarTime > Constants.MIN_STARSPAWN_WAIT)
	//    spawnStar();
	
	generator.generate(delta);
	
	Iterator<Star> starIterator = stars.iterator();
	while(starIterator.hasNext()) {
	    
	    Star star = starIterator.next();
	    
	    star.applySpeed(delta);
	    
	    if(star.rect.y + star.rect.height < 0)
		starIterator.remove();
	}
	
	Iterator<Asteroid> asteroidIterator = asteroids.iterator();
	while(asteroidIterator.hasNext()) {
	    
	    Asteroid asteroid = asteroidIterator.next();
	    
	    asteroid.applySpeed(delta);
	    
	    //System.out.println("" + asteroid.collisionPolygon.getX() + " " + asteroid.collisionPolygon.getY());
	    
	    if(asteroid.rect.y + asteroid.rect.height < 0 )
		asteroidIterator.remove();
	    
	    else if(spaceship.isVulnerable() && overlapsPolygonRect(asteroid.collisionPolygon, spaceship.collisionRectangle)) {
		    
		asteroidIterator.remove();
		
		onCollisionWithAsteroidOrLaser();
	    }
	}
	
	Iterator<EnemyProjectile> el = enemyProjectiles.iterator();
	while(el.hasNext()) {
	    
	    EnemyProjectile eLaser = el.next();
	    
	    eLaser.applySpeed(delta);
	    
	    if(spaceship.isVulnerable() && overlapsPolygonRect(eLaser.collisionPolygon, spaceship.collisionRectangle)) {
		
		el.remove();
		
		onCollisionWithAsteroidOrLaser();
	    }
	}
	
	Iterator<Enemy> enem = enemies.iterator();
	while(enem.hasNext()) {
	    
	    Enemy ufo = enem.next();
	    
	    ufo.applySpeed(delta);
	    
	    if(ufo.updateTimer(delta)) {
		
		shootPlayer(ufo.rect.x + ufo.rect.width / 2 - enemyLaserTexture.getWidth() / 2,
			    ufo.rect.y + ufo.rect.height / 2 - enemyLaserTexture.getHeight() / 2);
	    }
	    
	    if(ufo.rect.x > Constants.GAME_WIDTH || ufo.rect.x + ufo.rect.width < 0f) {
		
		enem.remove();
	    }
	}
	
	Iterator<Rectangle> laserIterator = lasers.iterator();
	while(laserIterator.hasNext()) {
	    
	    Rectangle laser = laserIterator.next();
	    
	    laser.y += Constants.PROJECTILE_SPEED * delta;
	    
	    if(laser.y > Constants.GAME_HEIGHT) {
		
		laserIterator.remove();
	    }
	    
	    else {
		
		boolean check = true;
		
		for(int i = 0; i < asteroids.size; ++i) {
		
		    if(overlapsPolygonRect(asteroids.get(i).collisionPolygon, laser)) {

			//TODO properly handle projectile - asteroid collision
			currentScore += Constants.ASTEROID_POINTS[asteroids.get(i).size.ordinal()];

			asteroids.removeIndex(i);
			laserIterator.remove();
			
			check = false;

			break;
		    }
		}
		
		if(check) {
		    
		    for(int i = 0; i < enemies.size; ++i) {
			
			if(laser.overlaps(enemies.get(i).rect)) {
			    
			    currentScore += Constants.ENEMY_POINTS;
			    
			    enemies.removeIndex(i);
			    laserIterator.remove();
			    
			    break;
			}
		    }
		}
	    }
	}
	
	//System.out.println("Number of stars: " + Integer.toString(stars.size));
    }
    
    @Override
    public void show() {
        
    }

    @Override
    public void render(float delta) {
	
	draw(delta);
	
	update(delta);
    }

    @Override
    public void resize(int width, int height) {
        
        game.view.resize(width, height);
    }

    @Override
    public void pause() {
        
        
    }

    @Override
    public void resume() {
        
        
    }

    @Override
    public void hide() {
	
	
    }

    @Override
    public void dispose() {
        
	spaceshipTexture.dispose();
	starsTexture.dispose();
	laserTexture.dispose();
	enemyLaserTexture.dispose();
	enemyTexture.dispose();
	asteroidTexture.dispose();
	heartTexture.dispose();
    }
    
    
}
