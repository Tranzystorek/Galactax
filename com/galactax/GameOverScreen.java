/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * A screen for a Game Over situation.
 * 
 * @see Screen
 * @author marcin
 */
public class GameOverScreen implements Screen {

    final Galactax game;
    
    Texture gameOverTexture;
    
    private final int gameScore;
    
    /**
     * Construct new Game Over screen
     * 
     * @param g
     * @param score score achieved in the last game
     */
    public GameOverScreen(final Galactax g, int score) {
	
	this.game = g;
	gameScore = score;
	
	gameOverTexture = new Texture("game_over.png");
    }
    
    @Override
    public void show() {
	
	
    }

    @Override
    public void render(float delta) {
	
	game.view.render(delta);
	
	game.view.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
	game.view.shapeRenderer.setColor(Constants.MENU_BACKGROUND_COLOR);
	game.view.shapeRenderer.rect(0, 0, Constants.GAME_WIDTH, Constants.GAME_HEIGHT);
	game.view.shapeRenderer.end();
	
	game.view.batch.begin();
	
	game.view.font.draw(game.view.batch, "GAME OVER", 150, 430);
	game.view.font.draw(game.view.batch, "Your score : " + gameScore, 150, 370);
	
	game.view.font.draw(game.view.batch, "Press ENTER", 150, 150);
	game.view.font.draw(game.view.batch, "to return to menu", 150, 110);
	
	game.view.batch.draw(gameOverTexture, Constants.GAME_WIDTH / 2 - gameOverTexture.getWidth() / 2, Constants.GAME_HEIGHT - gameOverTexture.getHeight() - 200);
	
	game.view.batch.end();
	
	if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
	    
	    game.launchMenu();
	}
    }

    @Override
    public void resize(int width, int height) {
	
	game.view.resize(width, height);
    }

    @Override
    public void pause() {
	
	
    }

    @Override
    public void resume() {
	
	
    }

    @Override
    public void hide() {
	
	
    }

    @Override
    public void dispose() {
	
	gameOverTexture.dispose();
    }
}
