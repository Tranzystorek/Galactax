/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Abstract blueprint for moving objects in the game.
 *
 * @author marcin
 */
public abstract class GameObject {
    
    public Rectangle rect;
    
    protected Vector2 speed;
    
    /**
     * Construct new object, specifying its movement rectangle.
     * 
     * @param x
     * @param y
     * @param w object's width in pixels
     * @param h object's height in pixels
     */
    public GameObject(float x, float y, float w, float h) {
	
	rect = new Rectangle(x, y, w, h);
    }
    
    /**
     * Apply the speed vector to the object in relation to passed time.
     * 
     * @param delta passed time portion in seconds
     */
    public void applySpeed(float delta) {
	
	rect.x += speed.x * delta;
	rect.y += speed.y * delta;
    }
}
