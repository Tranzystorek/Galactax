/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * 
 *
 * @author marcin
 * @see Screen
 */
public class SettingsScreen implements Screen {
    
    final Galactax game;
    
    Options option;
    
    enum Options {
	
	FULLSCREEN {
	    @Override
	    public Options prev() {
		
		return Options.BACK;
	    }
	},
	
	RESET_RES,
	
	BACK {
	    @Override
	    public Options next() {
		
		return Options.FULLSCREEN;
	    }
	};
	
	public Options next() {
	    
	    return values()[ordinal() + 1];
	}
	
	public Options prev() {
	    
	    return values()[ordinal() - 1];
	}
    }
    
    /**
     * Constructs a new SettingsScreen.
     * 
     * @param g reference to the main game class
     */
    public SettingsScreen(final Galactax g) {
	
	this.game = g;
    }

    @Override
    public void show() {
	
	option = Options.FULLSCREEN;
    }

    @Override
    public void render(float delta) {
	
	game.view.render(delta);
	
	game.view.shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
	game.view.shapeRenderer.setColor(Constants.MENU_BACKGROUND_COLOR);
	game.view.shapeRenderer.rect(0f, 0f, Constants.GAME_WIDTH, Constants.GAME_HEIGHT);
	game.view.shapeRenderer.end();
	
	game.view.batch.begin();
	
	game.view.font.draw(game.view.batch, "Toggle fullscreen", 150, 500);
	game.view.font.draw(game.view.batch, "Reset resolution", 150, 440);
	game.view.font.draw(game.view.batch, "Back", 150, 380);
	
	game.view.batch.draw(game.view.selectorTexture, 150 - 20 - game.view.selectorTexture.getWidth(),
							500 - 60 * option.ordinal() - game.view.selectorTexture.getHeight());
	
	game.view.batch.end();
	
	if(Gdx.input.isKeyJustPressed(Constants.SELECT_KEY)) {
	    
	    switch(option) {
		
		case FULLSCREEN:
		    game.view.toggleFullscreen();
		    break;
		
		case RESET_RES:
		    game.view.resetResolution();
		    break;
		
		case BACK:
		    
		    if(game.isPaused())
			game.pauseGame();
		    
		    else
			game.launchMenu();
		    
		    break;
	    }
	}
	
	if(Gdx.input.isKeyJustPressed(Constants.UP_KEY)) {
	    
	    option = option.prev();
	}
	
	else if(Gdx.input.isKeyJustPressed(Constants.DOWN_KEY)) {
	    
	    option = option.next();
	}
    }

    @Override
    public void resize(int width, int height) {
	
	game.view.resize(width, height);
    }

    @Override
    public void pause() {
	
	
    }

    @Override
    public void resume() {
	
	
    }

    @Override
    public void hide() {
	
	
    }

    @Override
    public void dispose() {
	
	
    }
}
